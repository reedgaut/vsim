import math
# Convert Miles per hour to meters per second
def mph_to_mps(mph):
    return mph * 0.44704

# Convert meters per second to miles per hour
def mps_to_mph(mps):
    return mps * 2.237

def calculate_distance(latlong1, latlong2):
    R = 6373.0  # radius of the Earth in km

    # coordinates
    lat1 = math.radians(latlong1[0])
    lon1 = math.radians(latlong1[1])
    lat2 = math.radians(latlong2[0])
    lon2 = math.radians(latlong2[1])

    # change in coordinates
    dlon = lon2 - lon1
    dlat = lat2 - lat1

    # Haversine formula
    a = math.sin(dlat / 2) ** 2 + math.cos(lat1) * math.cos(lat2) * math.sin(dlon / 2) ** 2
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    distance = R * c
    return distance

def thread_function(name):
    logging.info("Thread %s: starting", name)
    time.sleep(2)
    logging.info("Thread %s: finishing", name)